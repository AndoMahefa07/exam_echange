create database takalo;
use takalo;

create table user(
    id int primary key auto_increment,
    nom varchar(50),
    email varchar(50),
    password varchar(50)
);

create table category(
    id int primary key auto_increment,
    nom varchar(50)
);

create table objet(
    id int primary key auto_increment,
    nom varchar(50),
    description text,
    prix double precision,
    idcategory int,
    iduser int
);

create table photo(
    id int primary key auto_increment,
    nom varchar(50),
    idobjet int
);

create table proposition(
    id int primary key auto_increment,
    idobjet1 int,
    idobjet2 int
);

create table actionproposition(
    idproposition int,
    etat varchar(50)
);

alter table objet add foreign key (idcategory) references category(id);
alter table objet add foreign key (idcategory) references category(id);
alter table objet add foreign key (idcategory) references category(id);
alter table actionproposition add foreign key (idproposition) references proposition(id);
insert into user values (0,'root','root@gmail.com','root');
insert into category values (null,'Vêtement');
insert into category values (null,'DVD');
insert into category values (null,'Livre');
insert into category values (null,'Bijoux');

update user set id='0' where nom='root';

-- create or replace view allObjet as  Select user.id as idUser , objet.id as id_Object,objet.nom as Object_nom,objet.description as Object_description,objet.prix as Object_prix,photo.nom as Photo_nom,category.nom as Category_nom,category.id as idCategory FROM objet JOIN photo ON objet.id = photo.idobjet JOIN category ON objet.idcategory = category.id JOIN user ON user.id = objet.iduser ; 
-- create or replace view allObjetpre as  Select user.id as idUser,objet.id as idObject,objet.nom as Object_nom,objet.description as Object_description,objet.prix as Object_prix,photo.nom as Photo_nom,category.nom as Category_nom,category.id as idCategory FROM objet JOIN photo ON objet.id = photo.idobjet JOIN category ON objet.idcategory = category.id JOIN user ON user.id = objet.iduser ; 
-- create or replace view allinfoProposition as  select objet.idUser as idUser,proposition.id as idProposition,proposition.idobjet1 as Object1,proposition.idobjet2 as Object2,objet.nom as Object_nom,description as Object_description,prix as Object_prix FROM objet Join proposition on objet.id = proposition.idobjet1 ; 
-- create or replace view getObject2 as  select actionProposition.idproposition as idProposition,proposition.idobjet1 as object1,proposition.idobjet2 as object2 from proposition join actionProposition on proposition.id = actionProposition.idproposition;
