<div class="site-section subscribe bg-light">
      <div class="container">
        <form action="#" class="row align-items-center">
          <div class="col-md-5 mr-auto">
            <h2>Takalo-Takalo: Fait pour tous!</h2>
            <p>Un site vous permattant de re-initialiser votre garde-robe ou bien de donner a d'autres personnes l'opportunite de 
            gouter a vos merveilleux souvenirs.</p>
            <p>N'Hesitez pas a echanger vos affaires sans frais ni credit sur notre site! On attend plus que vous et Hop pour des nouvelles aventures avec des nouveaux affaires.</p>
          </div>
          <div class="col-md-6 ml-auto">
            <div class="d-flex">
              <input type="email" class="form-control" placeholder="Enter your email">
              <button type="submit" class="btn btn-secondary" ><span class="icon-paper-plane"></span>
            </div>
          </div>
        </form>
      </div>
</div>
<div class="footer">
      <div class="container">
        <div class="row">
          <div class="col-12">
            <div class="copyright">
                <p>
                    Landria Ranto Liantsoa ETU001781 
                </p>
                <p>
                  Razafimahatratra Ando ETU001896
                </p>
                <p> 
                  Mahery ETU001859
                </p>
                <p>
                    Copyright &copy;<script>document.write(new Date().getFullYear());</script> | Ce site a ete fait avec beaucoup d' <i class="icon-heart text-danger" aria-hidden="true"></i> par <a href="https://colorlib.com" target="_blank" >Nous</a>
                </p>
                <div class="ml-md-auto top-social d-none d-lg-inline-block">
                  <a href="#" class="d-inline-block p-3"><span class="icon-facebook"></span></a>
                  <a href="#" class="d-inline-block p-3"><span class="icon-twitter"></span></a>
                  <a href="#" class="d-inline-block p-3"><span class="icon-instagram"></span></a>
                </div>
            </div>
          </div>
        </div>
      </div>
</div>

	<script src="assets/js2/vendor/jquery-3.2.1.min.js"></script>
	<script src="assets/js2/jquery.slicknav.min.js"></script>
	<script src="assets/js2/slick.min.js"></script>
	<script src="assets/js2/main.js"></script>

	</body>
</html>
