<section class="hero__section">
		<div class="hero-slider">
			<div class="slide-item">
				<a class="fresco" href="<?php echo site_url("assets/images/Vetements/trunchNoir.jpg");?>" data-fresco-group="projects">
					<img src="<?php echo site_url("assets/images/Vetements/trunchNoir.jpg");?>" alt="">
				</a>	
			</div>
			<div class="slide-item">
				<a class="fresco"href="<?php echo site_url("assets/images/DVD/zootopie.jpg");?>" data-fresco-group="projects">
					<img src="<?php echo site_url("assets/images/DVD/zootopie.jpg");?>" alt="">
				</a>
			</div>
			<div class="slide-item">
				<a class="fresco" href="<?php echo site_url("assets/images/Vetements/se-vetement-mode-femme.jpg");?>" data-fresco-group="projects">
					<img src="<?php echo site_url("assets/images/Vetements/se-vetement-mode-femme.jpg");?>" alt="">
				</a>	
			</div>
			<div class="slide-item">
				<a class="fresco" href="<?php echo site_url("assets/images/Livres/L'art.jpg");?>" data-fresco-group="projects">
					<img src="<?php echo site_url("assets/images/Livres/L'art.jpg");?>" alt="">
					</a>
			</div>
			<div class="slide-item">
				<a class="fresco" href="<?php echo site_url("assets/images/Jouets/Bibil.jpg");?>" data-fresco-group="projects">
				<img src="<?php echo site_url("assets/images/Jouets/Bibil.jpg");?>" alt="">
			</a>	
			</div>
			<div class="slide-item">
				<a class="fresco" href="<?php echo site_url("assets/images/DVD/miraculous.jpg");?>" data-fresco-group="projects">
				<img src="<?php echo site_url("assets/images/DVD/miraculous.jpg");?>" alt="">
				</a>	
			</div>
		</div>
		<div class="hero-text-slider">
			<div class="text-item">
				<h2>Trunch Noir</h2>
				<p>Un Haut hyper style</p>
			</div>
			<div class="text-item">
				<h2>Zootopie</h2>
				<p>Dessin anime</p>
			</div>
			<div class="text-item">
				<h2>Ensemble vert</h2>
				<p>Hyper style et confortable</p>
			</div>
			<div class="text-item">
				<h2>L'art</h2>
				<p>Livre d'art</p>
			</div>
			<div class="text-item">
				<h2>Petite voiture</h2>
				<p>Ideal pour votre enfant</p>
			</div>
			<div class="text-item">
				<h2>Miraculous</h2>
				<p>Dessin anime</p>
			</div>
		</div>
</section>


