<?php
session_start();
$idUser = $_SESSION['id']['id'];
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>TakaloTakalo &mdash; Website By Us</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


  <link href="https://fonts.googleapis.com/css?family=B612+Mono|Cabin:400,700&display=swap" rel="stylesheet">

  <link rel="stylesheet" href="<?php echo site_url("assets/fonts/icomoon/style.css"); ?>">

  <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/jquery-ui.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/owl.carousel.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/owl.theme.default.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/owl.theme.default.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/jquery.fancybox.min.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/bootstrap-datepicker.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/fonts/flaticon/font/flaticon.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/aos.css"); ?>">
  <link href="<?php echo site_url("assets/css/jquery.mb.YTPlayer.min.css" );?>" media="all" rel="stylesheet" type="text/css"?>
  <link rel="stylesheet" href="<?php echo site_url("assets/css/style.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css/styles.css"); ?>">
  <link rel="stylesheet" href="<?php echo site_url("assets/css2/fresco.css"); ?>">
	<link rel="stylesheet" href="<?php echo site_url("assets/css2/slick.css"); ?>">

	<link rel="stylesheet" href="<?php echo site_url("assets/css2/style.css"); ?>">

</head>

<body data-spy="scroll" data-target=".site-navbar-target" data-offset="300">

<div class="site-wrap">
  <div class="site-mobile-menu site-navbar-target">
    <div class="site-mobile-menu-header">
      <div class="site-mobile-menu-close mt-3">
        <span class="icon-close2 js-menu-toggle"></span>
      </div>
    </div>
</div>
    
<div class="header-top">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-12 col-lg-6 d-flex">
        <a href="index.html" class="site-logo">Takalo-Takalo</a>
        <div class="logo"><img src="<?php echo site_url("assets/images/Logo/logo1.jpg"); ?>" alt=""></div>
        <a href="#" class="ml-auto d-inline-block d-lg-none site-menu-toggle js-menu-toggle text-black"><span class="icon-menu h3"></span></a>
      </div>
          
      <div class="col-12 col-lg-6 ml-auto d-flex">
        <form action="<?php echo site_url('homeController/search'); ?>" class="search-form d-inline-block" method="get">
          <div class="d-flex">
            <input type="text" class="form-control" placeholder="Mot Cle" name="critere">
            <select class="form-control" name="category">
              <?php 
                for($i=0; $i<count($category); $i++){ ?> 
                  <option value="<?php echo $category[$i]['id']; ?>"><?php echo $category[$i]['nom']; ?></option>
                <?php }
              ?>
            </select>
            <button type="submit" class="btn btn-secondary" ><span class="icon-search"></span></button>
          </div>
          
        </form> 
      </div>
    </div>
  </div>
  
  <div class="site-navbar py-2 js-sticky-header site-navbar-target d-none pl-0 d-lg-block" role="banner">
        <div class="container">
          <div class="d-flex align-items-center">
            <div class="mr-auto">
              <nav class="site-navigation position-relative text-right" role="navigation">
                <ul class="site-menu main-menu js-clone-nav mr-auto d-none pl-0 d-lg-block">
                  <li class="active"><a href="<?php echo base_url("welcome");?>" class="nav-link text-left">Home</a></li>

                  <li class="dropdown">
                    <button class="dropbtn">Categorie</button>
                    <ul class="dropdown-content">
                      <?php 
                        for($i=0; $i<count($category); $i++){ 
                          $idcategory = $category[$i]['id'];
                          ?>
                        
                            <center><li><a href="<?php echo site_url("Welcome/categories?idcate=$idcategory&idd=$idUser"); ?>"><?php echo $category[$i]['nom'];?></a></li></center>
                        <?php }
                      ?>
                    </ul>
                  </li>

                  <!-- <li><a href="<?php //echo site_url("welcome/categories");?>" class="nav-link text-left">Categories</a></li> -->
                  <li><a href="<?php echo site_url("welcome/business?idUser=$idUser");?>" class="nav-link text-left">Business</a></li>
                  <li><a href="<?php echo site_url("welcome/contact");?>" class="nav-link text-left">Contact</a></li>
                  <li><a href="<?php echo site_url("welcome/profils");?>" class="nav-link text-left">Profils</a></li>
                  <li><a href="<?php echo site_url("welcome/history");?>" class="nav-link text-left">Historiques</a></li>
                  <li><a href="<?php echo site_url("welcome/statistiques");?>" class="nav-link text-left">Statistiques</a></li>
                  <li><a href="<?php echo site_url("loginController/deconnexion");?>" class="nav-link text-left">Deconnexion</a></li>
                </ul> 
              </nav>
            </div>
          </div>
          <div class="inserer"><a href="<?php echo site_url("welcome/insertion");?>" class="nav-link text-left">INSERER DES OBJETS?</a></div>
      </div>
    </div>   
    <hr> 
  </div>