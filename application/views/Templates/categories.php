<div class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-9">
            <div class="section-title">
              <span class="caption d-block small">Categories</span>
            </div>  
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="post-entry-1">
                <a href="post-single.html"><img src="<?php echo site_url("assets/images/DVD/Classiques.jpg");?>" alt="Image" class="img-fluid"></a>
                <h2><a href="blog-single.html">Plusieurs DVD a echanger les gens!</a></h2>
                <p>Je voulais echanger mes DVD des annees 1900 par des plus modernes</p>
                <div class="post-meta">
                  <span class="d-block"><a href="#">Dave Rogers</a> in <a href="#">News</a></span>
                  <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span class="icon-star2"></span></span>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <?php  
                $idU = $_SESSION['id']['id'];
                $idcate = $_GET['idcate'];
                for($i = 0; $i<count($nomCat); $i++) { 
                  $idt = $nomCat[$i]['idObjet']; ?>
              <div class="post-entry-2 d-flex bg-light">
                <div class="thumbnail" style="background-image: url('<?php echo site_url("assets/images/DVD/LesAnimauxFantastiques.jpg");?>')"></div>
                <div class="contents">
                  <h2><a href="blog-single.html"><?php echo $nomCat[$i]['nom']?></a></h2>
                  <div class="post-meta">
                    <span class="d-block"><a href="<?php echo site_url("seePropositionController?id=$idcate&idUser=$idU");?>"><button>See Proposition</button></a> in <a href="<?php echo site_url("propositionController?id=$idt&idUser=$idU")?>"><button>Sell</button></a></span>
                    <span class="date-read">Jun 14 <span class="mx-1">&bullet;</span> 3 min read <span class="icon-star2"></span></span>
                  </div>
                  <h5><?php
                    echo "Déscription :" . $nomCat[$i]['description'];
                    echo "<br>";
                    echo "<br>";
                    echo "Prix Estimation:" . $nomCat[$i]['prix'] . "Ar";
                  ?></h5>
                </div>
              </div>
              <?php } ?>
                         </div>    
          </div>
      </div>
    </div>

    <div class="site-section subscribe bg-light">
      <div class="container">
        <form action="#" class="row align-items-center">
          <div class="col-md-5 mr-auto">
            <h2>Newsletter Subcribe</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Perferendis aspernatur ut at quae omnis pariatur obcaecati possimus nisi ea iste!</p>
          </div>
          <div class="col-md-6 ml-auto">
            <div class="d-flex">
              <input type="email" class="form-control" placeholder="Enter your email">
              <button type="submit" class="btn btn-secondary" ><span class="icon-paper-plane"></span></button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <script src="js/jquery-3.3.1.min.js"></script>
  <script src="js/jquery-migrate-3.0.1.min.js"></script>
  <script src="js/jquery-ui.js"></script>
  <script src="js/popper.min.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/owl.carousel.min.js"></script>
  <script src="js/jquery.stellar.min.js"></script>
  <script src="js/jquery.countdown.min.js"></script>
  <script src="js/bootstrap-datepicker.min.js"></script>
  <script src="js/jquery.easing.1.3.js"></script>
  <script src="js/aos.js"></script>
  <script src="js/jquery.fancybox.min.js"></script>
  <script src="js/jquery.sticky.js"></script>
  <script src="js/jquery.mb.YTPlayer.min.js"></script>

  <script src="js/main.js"></script>
