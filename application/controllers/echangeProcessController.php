<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
class echangeProcessController extends CI_Controller {

	public function index()
	{	
        $idtest = $this->input->get('acc');
        $idob = $this->input->get('idpro');
        $idUser2 = $this->input->get('idUser');
        if($idtest == 0){
            $ok = "non";
            $this->load->model('Functions');
            $this->Functions->insertActionProcess($idob,$ok);
            $data['allObject'] = $this->Functions->validationProcess($idob); 
        }else if($idtest==1){
            $ok = "oui";
            $this->load->model('Functions');
            $this->Functions->insertActionProcess($idob,$ok);
            $data= $this->Functions->validationProcess($idob);
            $obj1 = $data[0]['Object1'];
            $obj2 = $data[0]['Object2'];
            $idUser1 = $_SESSION['id']['id'];
            $this->Functions->changeProcess($obj1,$obj2,$idUser1,$idUser2);
        }

	}
}