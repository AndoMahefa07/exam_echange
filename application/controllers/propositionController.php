<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class propositionController extends CI_Controller {

	public function index()
	{	
        $data['idObject'] = $this->input->get('id');
        $id = $this->input->get('idUser');
        $this->load->model('Functions');
		$data['allObject'] = $this->Functions->allObjetById($id);
		$this->load->view('Templates/header');
		$this->load->view('checkProposition',$data);
		$this->load->view('Templates/footer');
	}
}