<?php
defined('BASEPATH') OR exit('No direct script access allowed');
session_start();
class HomeController extends CI_Controller {
	public function index (){
		if(!isset($_SESSION['id'])){
			redirect('LoginController/index');
		}
        redirect('Welcome');
    }		

	public function search(){
		$critere = $this->input->get("critere");
		$idCate = $this->input->get("category");
		$this->load->model('Functions');
		$data['search'] = $this->Functions->search($idCate,$critere);
		$this->load->view('Templates/Search',$data);
	}
}