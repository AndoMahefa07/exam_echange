<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class ValiderPropositionController extends CI_Controller {

	public function index()
	{	
        $idobj1 = $this->input->get('idObject1');
        $idobj2 = $this->input->get('idObject2');

        $this->load->model('Functions');
		$data['allObject'] = $this->Functions->insertPropos($idobj1,$idobj2);
        redirect('welcome/home');
	}
}