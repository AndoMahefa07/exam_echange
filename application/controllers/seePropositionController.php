<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class seePropositionController extends CI_Controller {

	public function index()
	{	
        $idob = $this->input->get('id');
        $idUser = $this->input->get('idUser');

        $this->load->model('Functions');
		$data['allObject'] = $this->Functions->getProposition($idob);

        $this->load->view('seeProposition',$data);

	}
}