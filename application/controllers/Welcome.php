<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function index() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/home');
		$this->load->view('Templates/footer');
	}
	public function home() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/home');
		$this->load->view('Templates/footer');
	}
	public function categories() {	
		$idd = $this->input->get('idd');
		$id = $this->input->get('idcate');
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$data['nomCat'] = $this->Functions->objetByIdCat($id,$idd);
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/categories',$data);
		$this->load->view('Templates/footer');
	}		
	public function business() {	
		$id = $this->input->get('idUser');
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$data['allObject'] = $this->Functions->allObjetById($id);
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/business');
		$this->load->view('Templates/footer');
	}
	public function contact() {
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/contact');
		$this->load->view('Templates/footer');
	}
	public function profils() {	
		$this->load->model('Functions');
		$data['membre'] = $this->Functions->getMembre();
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/profils');
	}
	public function history() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/historiques');
		$this->load->view('Templates/footer');
	}
	public function statistiques() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$data['membre'] = $this->Functions->getMembre();
		$this->load->view('Templates/header',$data);
		$this->load->view('Templates/statistiques');
		$this->load->view('Templates/footer');	
	}
	public function insertion() {	
		$this->load->model('Functions');
		$data['category'] = $this->Functions->category();
		$this->load->view('Templates/insertObjet',$data);
	}		
}