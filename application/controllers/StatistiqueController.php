<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class StatistiqueController extends CI_Controller {
    public function stat(){
        $this->load->model('Functions');
        $data['membre'] = $this->Functions->getMembre();
        // $data['proposition'] = $this->Functions->proposition_accepte();
        $this->load->view('statistique',$data);
    }
}
?>