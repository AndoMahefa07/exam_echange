<?php
    defined('BASEPATH') or exit('No direct access');
    class Functions extends CI_Model {
        public function connexion($mail) {
            $requete =  "select * from user where email='".$mail."'";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $array = array(
                    'id' => $res['id'],
                    'nom' => $res['nom'],
                    'mail' => $res['email'],
                    'mdp' => $res['password']
                );
            }
            return $array;
        }
        
        public function getMembre() {
            $requete =  "select * from user";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    'id' => $res['id'],
                    'nom' => $res['nom'],
                    'mail' => $res['email'],
                    'mdp' => $res['password']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function inscription($nom,$mail,$mdp){
            $requete = "insert into user values (default,'%s','%s','%s')";
            $requete = sprintf($requete,$nom,$mail,$mdp);
            $this->db->query($requete);
        }
        
        public function getId($mail){
            $requete = "select id from user where email='".$mail."'";
            $result = $this->db->query($requete);
            $id = $result->row_array();
            return $id;
        }

        public function updatePassword($id,$mdp){
            $requete = "update user set password='".$mdp."' where id='".$id."'";
            $this->db->query($requete);
        }
        public function objets(){
            $requete = "select * from objet";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['id'],
                    "nom" => $res['nom'],
                    "desc" => $res['description'],
                    "prix" => $res['prix'],
                    "idcate" => $res['idcategory'],
                    "iduser" => $res['iduser']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function objetById($id){
            $requete = "select * from objet where id != ".$id;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['id'],
                    "nom" => $res['nom'],
                    "desc" => $res['description'],
                    "prix" => $res['prix'],
                    "idcate" => $res['idcategory'],
                    "iduser" => $res['iduser']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function insertObjet($nom,$description,$prix,$idcate,$iduser){
            $requete = "insert into objet values(default,'%s','%s',%s,%s,%s)";
            $requete = sprintf($requete,$nom,$description,$prix,$idcate,$iduser);
            $this->db->query($requete);
        }

        public function Objet_getidbyNom($name){
            $requete = "select id from objet where nom='%s'";
            $requete = sprintf($requete,$name);
            $result = $this->db->query($requete);
            $id = $result->row_array();
            return $id;
        }

        public function uploadPhoto($files,$id){
            $dossier = 'assets/upload';
            foreach($files['name'] as $key => $value) {
              $fileName = $files['name'][$key];
              $fileTmpName = $files['tmp_name'][$key];
              $fileSize = $files['size'][$key];
              $fileError = $files['error'][$key];
              $fileType = $files['type'][$key];
          
              $fileExt = explode('.', $fileName);
              $fileActualExt = strtolower(end($fileExt));
          
              $allowed = array('jpg', 'jpeg', 'png', 'svg');
          
              if(in_array($fileActualExt, $allowed)) {
                if($fileError === 0) {
                  if($fileSize < 1000000) {
                    $fileDestination = $dossier.$fileName;
                    move_uploaded_file($fileTmpName, $fileDestination);
                    $sql = "insert into photo values(default ,'%s', %s)";
                    $answer=sprintf($sql ,$fileName,$id) ;
                    $this->db->query($answer);
                  } else {
                    echo "Your file is too big!";
                  }
                } else {
                  echo "There was an error uploading your file!";
                }
              } else {
                echo "You cannot upload files of this type!";
              }
            }
          
        }

        public function category(){
            $requete = "select * from category";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['id'],
                    "nom" => $res['nom']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function categoryById($id){
            $requete = "select * from category where id=".$id;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['id'],
                    "nom" => $res['nom']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function idCate($nom){
            $requete = "select id from category where nom=".$nom;
            $result = $this->db->query($requete);
            $id = $result->row_array();
            return $id;
        }

        function Search($id,$critere){
            $requete = "select * from recherche where idCate=".$id." and nomCate like '%".$critere."%' or nom like '%".$critere."%' or description like '%".$critere."%'";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "idobjet" => $res['id'],
                    "nomObjet" => $res['nom'],
                    "descObjet" => $res['description'],
                    "prixObjet" => $res['prix'],
                    "idCate" => $res['idcategory'],
                    "idUser" => $res['iduser'],
                    "nomCate" => $res['nomCate']
                );
                array_push($array,$ar);
            } 
            return $array;
        }
  
        public function delete($id){
            $requete = "delete from objet where id=".$id;
            $this->db->query($requete);
        }

        public function proposition_accepte(){
            $requete = "select * from actionproposition where etat=oui";
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['idproposition'],
                    "etat" => $res['etat']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function insertPropos($prop1,$prop2){
            $requete = "insert into proposition values (default,'%s','%s')";
            $requete = sprintf($requete,$prop1,$prop2);
            $this->db->query($requete);
        }

        public function getProposition($idObjet ){
            $requete = "select objet.idUser as idUser,proposition.id as idProposition,proposition.idobjet1 as Object1,proposition.idobjet2 as Object2,objet.nom as Object_nom,description as Object_description,prix as Object_prix FROM objet Join proposition on objet.id = proposition.idobjet1 where proposition.idobjet2=".$idObjet;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "idUser" => $res['idUser'],
                    "id" => $res['idProposition'],
                    "nom" => $res['Object_nom'],
                    "description" => $res['Object_description'],
                    "prix" => $res['Object_prix']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function objetByIdCat($id,$idU){
            $requete = "Select user.id as idUser , objet.id as id_Object,objet.nom as Object_nom,objet.description as Object_description,objet.prix as Object_prix,photo.nom as Photo_nom,category.nom as Category_nom,category.id as idCategory FROM objet JOIN photo ON objet.id = photo.idobjet JOIN category ON objet.idcategory = category.id JOIN user ON user.id = objet.iduser where idCategory =".$id." and idUser =".$idU;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "idObjet" => $res['id_Object'],
                    "nom" => $res['Object_nom'],
                    "description" => $res['Object_description'],
                    "prix" => $res['Object_prix'],
                    "photo" => $res['Photo_nom']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function allObjetById($id){
            $requete = "Select user.id as idUser,objet.id as idObject,objet.nom as Object_nom,objet.description as Object_description,objet.prix as Object_prix,photo.nom as Photo_nom,category.nom as Category_nom,category.id as idCategory FROM objet JOIN photo ON objet.id = photo.idobjet JOIN category ON objet.idcategory = category.id JOIN user ON user.id = objet.iduser where idUser != ".$id;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "id" => $res['idObject'],
                    "nom" => $res['Object_nom'],
                    "description" => $res['Object_description'],
                    "prix" => $res['Object_prix'],
                    "photo" => $res['Photo_nom']
                );
                array_push($array,$ar);
            }
            return $array;
        }

        public function validationProcess($idProposition){
            $requete = "select actionProposition.idproposition as idProposition,proposition.idobjet1 as object1,proposition.idobjet2 as object2 from proposition join actionProposition on proposition.id = actionProposition.idproposition where idproposition =".$idProposition;
            $result = $this->db->query($requete);
            $array = array();
            foreach($result->result_array() as $res){
                $ar = array(
                    "Object1" => $res['object1'],
                    "Object2" => $res['object2']
                );
                array_push($array,$ar);
            }
            return $array;
        }
    
        public function changeProcess($obj1,$obj2,$iduser1,$iduser2){
            
            $requete = "update objet set iduser = '%s' where id = '%s' ";
            $requete = sprintf($requete,$iduser2,$obj2);
            $this->db->query($requete);
            $requete = "update objet set iduser = '%s' where id = '%s' ";
            $requete = sprintf($requete,$iduser1,$obj1);
            $this->db->query($requete);
        }
    
        public function insertActionProcess($idProposition,$message){
            $requete = "insert into actionProposition values ('%s','%s')";
            $requete = sprintf($requete,$idProposition,$message);
            $this->db->query($requete);
        }
    }
?>